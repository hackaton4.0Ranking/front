﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton.Shared
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "El nombre de usuario es requerido")]
        public string Username { get; set; } = string.Empty;

         [Required(ErrorMessage = "La contraseña es requerida")]
         [StringLength(8, ErrorMessage = "La contraseña debe tener al menos {2} caracteres de longitud.")]
        public string Password { get; set; } = string.Empty;

         [Required(ErrorMessage = "Confirmar contraseña es requerido")]
         [Compare("Password", ErrorMessage = "Las contraseñas no coinciden")]
        public string ConfirmPassword { get; set; } = string.Empty;
    }
}
