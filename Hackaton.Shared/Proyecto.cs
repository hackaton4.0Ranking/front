﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hackaton.Shared
{
    public class Proyecto
    {
        public int IdProyecto { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 50)]
        public string? Nombre { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [StringLength(maximumLength: 255)]
        public string? Descripcion { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public DateTime? FechaInicio { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public DateTime? FechaFin { get; set; }

        public List<Usuario>? Usuarios { get; set; }
    }
}
