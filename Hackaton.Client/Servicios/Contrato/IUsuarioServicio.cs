﻿using Hackaton.Shared;

namespace Hackaton.Client.Servicios.Contrato
{
    public interface IUsuarioServicio
    {
        Task<bool> AutenticarUsuario(Usuario Usuario);
        Task<bool> CrearUsuario(Usuario Usuario);
        Task<bool> CerrarSesion(Usuario Usuario);
    }
}
