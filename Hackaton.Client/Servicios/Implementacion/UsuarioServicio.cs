﻿using Hackaton.Client.Servicios.Contrato;
using Hackaton.Shared;
using System.Net.Http.Json;
using static System.Net.WebRequestMethods;

namespace Hackaton.Client.Servicios.Implementacion
{
    public class UsuarioServicio: IUsuarioServicio
    {
        private readonly HttpClient _httpClient;
        public UsuarioServicio(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public Task<bool> AutenticarUsuario(Usuario Usuario)
        {
            throw new NotImplementedException();
        }

        public Task<bool> CerrarSesion(Usuario Usuario)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> CrearUsuario(Usuario Usuario)
        {
            var response = await _httpClient.PostAsJsonAsync("http://192.168.0.17:8080/registro", Usuario);
            //var result = await _httpClient.PostAsJsonAsync("api/Usuario", Usuario);
            //var response = await result.Content.ReadFromJsonAsync<ResponseAPI<int>>();

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Usuario registrado exitosamente");
                // Redirigir al usuario a la página de inicio de sesión u otra página
            }
            else
            {
                Console.WriteLine($"Error al registrar usuario: {response.StatusCode}");
                // Manejar el error adecuadamente
            }

            throw new NotImplementedException();
        }

    }
}
